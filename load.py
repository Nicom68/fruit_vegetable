import pandas as pd
import mysql.connector
import openpyxl
from mysql.connector import Error, IntegrityError





# Connexion à la base de données "fruit_vegetable"
mydb = mysql.connector.connect(
    host="localhost",
    user="nicolas",
    password="DbxsvFb56K52A7;",
    database="fruit_vegetable"
)

 
# création du curseur
cursor = mydb.cursor()

xlsx = pd.ExcelFile("pivot-tables.xlsx")
df = pd.read_excel(xlsx, "Sheet1")



df_reset = df.set_index('Order ID')


for index, row in df_reset.iterrows():
    country_name = row["Country"]
    cursor.execute("SELECT country_id FROM country WHERE country_name = %s", (country_name,))
    country_id = cursor.fetchone()
    if not country_id:
        sql1 = "INSERT INTO country (country_name) VALUES (%s)"
        val1 = (row ["Country"],)
        cursor.execute(sql1, val1)
        mydb.commit()
        country_id = cursor.lastrowid
        
    else:
        country_id = country_id[0]
     


for index, row in df_reset.iterrows():
    product_name = row["Product"]
    cursor.execute("SELECT product_id FROM product WHERE product_name = %s", (product_name,))
    product_id = cursor.fetchone()
    if not product_id:
        sql2 = "INSERT INTO product (product_name, category) VALUES (%s, %s)"
        val2 = (row["Product"], row["Category"])
        cursor.execute(sql2, val2)
        mydb.commit()
        product_id = cursor.lastrowid
    else:
        product_id = product_id[0]
        

for index, row in df_reset.iterrows():
    sql3="""INSERT INTO orders (amount, date, country_id, product_id) VALUES (%s, %s, %s, %s);"""
    val3= (row["Amount"],row ["Date"], country_id, product_id)
    cursor.execute(sql3,val3)
    mydb.commit()




cursor.close()
mydb.close()