import pandas as pd
import mysql.connector
import openpyxl
import numpy as np


xlsx = pd.ExcelFile("pivot-tables.xlsx")
df = pd.read_excel(xlsx, "Sheet1")


df_reset = df.set_index('Order ID')


# Nombre de commandes par produit et par pays

table = pd.pivot_table(df_reset, values='Amount', index=['Country', 'Product'],
                       columns= None, aggfunc="count")



# Montant des commandes par pays et par produit
country_order = pd.pivot_table(df_reset, values='Amount', index=['Country'],
                       columns= ['Product'], aggfunc="sum", margins=True, margins_name='Total')


# fonction pour récupérer le nombre de commandes par produit et par pays

def get_tcd(df_reset):
    table = pd.pivot_table(df_reset, values='Amount', index=['Country', 'Product'],
                            columns=None, aggfunc='count')
    return table

# fonction pour récupérer le montant des commandes par pays et par produit
def get_tcd(df_reset):
    country_order = pd.pivot_table(df_reset, values='Amount', index=['Country'],
            columns= ['Product'], aggfunc="sum", margins=True, margins_name='Total')
    return country_order

# exemple
result = get_tcd(df_reset)
print(result.loc["France", "Apple"])

