DROP DATABASE IF EXISTS fruit_vegetable;
CREATE DATABASE fruit_vegetable;
USE fruit_vegetable;

GRANT ALL PRIVILEGES ON fruit_vegetable.* TO 'nicolas'@'localhost';



CREATE TABLE country (
country_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
country_name varchar(255)
);

CREATE TABLE product (
product_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
product_name varchar(255),
category enum ("Fruit","Vegetables")
);


CREATE TABLE orders (
order_id int AUTO_INCREMENT PRIMARY KEY,
amount int,
date date,
country_id INT REFERENCES country.country_id,
product_id INT REFERENCES product.product_id
);


